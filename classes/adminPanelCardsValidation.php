<?php

class AdminPanelCardsValidation
{
    public static function validateEmptyField($picture, $title, $subtitle, $website, $desc)
    {
        if(empty($picture) || empty($title) || empty($subtitle) || empty($website) || empty($desc))
        {
            return false;
        } 
        return true;
    }
    public static function validateUrl($url)
    {
        if(!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$url))
        {
            return false;
        }
        return true;
    }

}