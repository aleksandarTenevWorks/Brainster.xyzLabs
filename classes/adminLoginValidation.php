<?PHP


class AdminLoginValidation
{
    private $errorUser = '';
    private $errorPass = '';
    private $user = 'admin';
    private $password = 'admin';

    public function validateHashedPass($pass)
    {
        $password_encrypted = password_hash($this->password, PASSWORD_DEFAULT);
        if (password_verify($pass, $password_encrypted)) 
        {
            return true;
        }
        return false;
        
    }
    public function validateUser($user)
    {
        if($user == $this->user)
        {
            return true;
        }
        return false;
    }
    public function validateAdminLogin($user,$pass)
    {
        if($this->validateUser($user) == false)
        {
            $this->errorUser = 'Wrong Username';
        }
        if($this->validateHashedPass($pass) == false)
        {
            $this->errorPass = 'Wrong Password';
        }
        if($this->validateUser($user) == true && $this->validateHashedPass($pass) == true)
        {
            session_start();
            $_SESSION['logged'] = "admin";
            header('Location:components/addProjectPage.php');
        }
    }
    public function getErrorUser()
    {
        return $this->errorUser;
    }
    public function getErrorPass()
    {
        return $this->errorPass;
    }
}
