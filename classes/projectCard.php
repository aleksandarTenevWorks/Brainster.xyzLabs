<?php
$dbConection = "/var/www/html/Brainster Labs/connection/Connection.php";
include_once ($dbConection);

class ProjectCard 
{
    private $picture;
    private $title;
    private $subtitle;
    private $description;
    private $website;

    public function __construct($picture = null , $title = null, $subtitle = null, $description = null,  $website = null)
    {
        $this->picture = $picture;
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->description = $description;
        $this->website = $website;
    }
    public function listAll()
    {
        $tableCompany = new Connection();
        $db = $tableCompany->connection();
        $sql = "SELECT * FROM Projects;";
        $stm = $db->query($sql);
        return $stm->fetchAll();
    }
    public function findRow($id)
    {
        $tableCompany = new Connection();
        $db = $tableCompany->connection();
        $sql = "SELECT * FROM Projects WHERE id = :id";
        $stm = $db->prepare($sql);
        $stm->bindParam(':id' , $id);
        $stm->execute();
        return $stm->fetch();
    }
    public function updateRow($id, $picture, $title, $subtitle, $description, $website)
    {
        $tableCompany = new Connection();
        $db = $tableCompany->connection();
        $sql = "UPDATE Projects
                SET picture_url=:picture, title=:title, subtitle=:subtitle, description=:description, website_url=:website
                WHERE id=:id";
        $stm = $db->prepare($sql);
        $stm->bindParam(':picture' , $picture);
        $stm->bindParam(':title' , $title);
        $stm->bindParam(':subtitle' , $subtitle);
        $stm->bindParam(':description' , $description);
        $stm->bindParam(':id' , $id);
        $stm->bindParam(':website' , $website);
        return $stm->execute(); 
    }
    public function deleteRow($id)
    {
        $tableCompany = new Connection();
        $db = $tableCompany->connection();
        $sql = "DELETE FROM Projects 
                WHERE id=:id";
        $stm = $db->prepare($sql);
        $stm->bindParam(':id' , $id);
        return $stm->execute(); 
    }
    public function addToDb()
    {
        $tableCompany = new Connection();
        $db = $tableCompany->connection();
        $sql = "INSERT INTO Projects(picture_url,title,subtitle ,description, website_url) VALUES (:picture , :title , :subtitle , :description, :website);";
        $stm = $db->prepare($sql);
        $stm->bindParam(':picture' , $this->picture);
        $stm->bindParam(':title' , $this->title);
        $stm->bindParam(':subtitle' , $this->subtitle);
        $stm->bindParam(':description' , $this->description);
        $stm->bindParam(':website' , $this->website);
        return $stm->execute(); 
    }
}