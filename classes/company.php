<?php
require_once 'connection/Connection.php';

class Company
{
    private $email;
    private $number;
    private $name;


    public function __construct($email , $number , $name)
    {
        $this->email = $email;
        $this->number = $number;
        $this->name = $name;
    }

    public function saveCompanyDatabase()
    {
        $tableCompany = new Connection();
        $db = $tableCompany->connection();
        $sql = "INSERT INTO Companies(email,number,company_name) VALUES (:email , :number , :companyName )";
        $stm = $db->prepare($sql);
        $stm->bindParam(':email' , $this->email );
        $stm->bindParam(':number' , $this->number );
        $stm->bindParam(':companyName' , $this->name );
        return $stm->execute();
    }
}