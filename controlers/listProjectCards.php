<?php

    require_once 'classes/projectCard.php';

    $allProjects = new ProjectCard();
    $arrayProjects = $allProjects->listAll();
?>
<div class="cardWrapper">
    <?php
        foreach($arrayProjects as $value)
        {
    ?>
            <div class="card">
                <div class="cardPic text-center">
                    <a href="<?= $value['website_url'] ?>" target="_blank"><img src=" <?= $value['picture_url'] ?>"></a>
                </div>
                <h2 class="cardTitle"><?= $value['title'] ?> </h2> 
                <h3> <?=  $value['subtitle']; ?> </h3>
                <p class="cardDesc"><?= $value['description'] ?> </p>
            </div>
    <?php
        }
    ?>
</div>