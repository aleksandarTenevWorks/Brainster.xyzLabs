<?php
    $adminLoginValidation = "/var/www/html/Brainster Labs/classes/adminLoginValidation.php";
    require_once ($adminLoginValidation);
    $passwordHash = "/var/www/html/Brainster Labs/classes/passwordHash.php";


    $validate = new AdminLoginValidation();

    if(isset($_POST['adminLoginBtn']))
    {
        $userAdmin = $_POST['user'];
        $passAdmin = $_POST['pass'];
        $validate->validateAdminLogin($userAdmin,$passAdmin);
    }