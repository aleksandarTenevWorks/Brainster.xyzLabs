<?php

    require_once '../classes/projectCard.php';

    $listAllProjectsRadio = new ProjectCard();
    $arrayProjects = $listAllProjectsRadio->listAll();
    usort($arrayProjects, function($a, $b) 
                            {
                                return $a['title'] > $b['title'] ;
                            });
    $succesNote = '';

    if(isset($_POST['updateProjectBtn']))
    {
        $id = $_POST['id'];
        $picture = $_POST['updatePicture'];
        $title = $_POST['updateTitle'];
        $subtitle = $_POST['updateSubtitle'];
        $description = $_POST['updateDescription'];
        $website = $_POST['updateWebsite'];
        $listAllProjectsRadio->updateRow($id, $picture, $title, $subtitle, $description, $website);
        header("Location:updateDeleteProject.php?status=updateSucces");
    }
    if(isset($_POST['delete']))
    {
        $rowID = (int) $_POST['project'];
        $listAllProjectsRadio->deleteRow($rowID);
        header("Location:updateDeleteProject.php?status=deleteSucces");
    }

    if(isset($_GET['status']))
    {
        if($_GET['status'] == 'updateSucces')
        {
            $succesNote .= 'Project Succesfully Updated';
        } elseif ($_GET['status'] == 'deleteSucces')
        {
            $succesNote .= 'Project Succesfully Deleted';
        }
    }