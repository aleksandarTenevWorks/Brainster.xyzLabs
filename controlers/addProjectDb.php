<?php
    require_once '../classes/projectCard.php';
    require_once '../classes/adminPanelCardsValidation.php';



    $succesNote = "";
    $emptyFieldError = '';
    $wrongUrlFormat = '';

    if(isset($_POST["addProjectBtn"]))
    {
        $picture = $_POST['picture'];
        $title = $_POST['title'];
        $subtitle = $_POST['subtitle'];
        $desc = $_POST['description'];
        $website = $_POST['website'];
        $checkEmpty = AdminPanelCardsValidation::validateEmptyField($picture, $title, $subtitle, $website, $desc);
        $checkUrl = AdminPanelCardsValidation::validateUrl($website);
        $errorEmpty = '';
        $errorUrl = '';

        if($checkEmpty == false)
        {
            $errorEmpty = 'error';
        }
        if($checkUrl == false)
        {
        $errorUrl = 'error';        
        }
        if($errorEmpty != '' || $errorUrl != '' )
        {
            header("location:addProjectPage.php?errorEmpty=". $errorEmpty. "&errorUrl=" . $errorUrl . "");
        }
        if($checkEmpty && $checkUrl)
        {
            $newProject = new ProjectCard($picture , $title , $subtitle , $desc, $website);
            if($newProject->addToDb())
            {
                header("location:addProjectPage.php?status=succes");
            }
        }
    }

    if(isset($_GET['status'])){
        
        if($_GET['status'] == 'succes')
        {
            $succesNote .= 'Project Succesfully Added';
        } else
        {
            $succesNote .= 'Project not added, please try again';
        }
    }
    if(isset($_GET['errorEmpty']) && $_GET['errorEmpty'] == 'error')
    {
        $emptyFieldError = 'Input field can not be empty';
    }
    if(isset($_GET['errorUrl']) && $_GET['errorUrl'] == 'error')
    {
        $wrongUrlFormat = 'Url format should be "https://www.example.com"';
    }