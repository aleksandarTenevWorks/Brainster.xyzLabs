<?php
    require_once 'classes/company.php';

    $errorEmail = '';
    $errorEmpty = '';

    if(isset($_POST['submit']))
    { 
        $email = $_POST['email'];
        $contact = $_POST['contact'];
        $companyName = $_POST['companyName'];

        if(empty($email) || empty($contact) || empty($companyName))
        {
            $errorEmpty = 'Input fields can not be empty';
        } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            $errorEmail = 'Invalid Email';  
        } else 
        {
            $saveCompany = new Company($email,$contact,$companyName);
            $saveCompany->saveCompanyDatabase();
            header('Location: brainsterLab.php?sendSucces=Thank you for the response');
        }   
    }
    if($errorEmail != '' || $errorEmpty != '')
    {
        header("Location: brainsterLab.php?emailError=". $errorEmail. "&emptyError=" . $errorEmpty . "");
    }



