-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 19, 2018 at 07:26 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.2.8-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `brainsterLab`
--

-- --------------------------------------------------------

--
-- Table structure for table `Companies`
--

CREATE TABLE `Companies` (
  `id` int(11) NOT NULL,
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Projects`
--

CREATE TABLE `Projects` (
  `id` int(11) NOT NULL,
  `picture_url` text COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `subtitle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website_url` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Projects`
--

INSERT INTO `Projects` (`id`, `picture_url`, `title`, `subtitle`, `description`, `website_url`) VALUES
(53, 'http://pogon.co/wp-content/uploads/2015/12/Brainster-Logo.png', 'Браинстер', 'Онлајн академија', 'Што сакате да научите денес? Најдете ги најдобрите курсеви по: бизнис, програмирање, маркетинг, дизајн.', 'www.brainster.co/'),
(97, 'http://www.stickpng.com/assets/images/580b57fcd9996e24bc43c529.png', 'Netflix', 'Movies And Series', 'Watch Netflix movies & TV shows online or stream right to your smart TV, game console, PC, Mac, mobile, tablet and more.Watch Netflix movies & TV shows online or stream right to your smart TV, game co', 'https://www.netflix.com/mk/'),
(98, 'http://diylogodesigns.com/blog/wp-content/uploads/2016/05/Pizza-Hut-Logo-PNG-Transparent-Background.png', 'Pizza Hut', 'Food and restaurant', 'Order pizza online for fast delivery or carryout from a store near you. View our full menu, see nutritional information, find store locations, and more.', 'https://twitter.com/pizzahut?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor'),
(99, 'http://www.stickpng.com/assets/images/580b57fcd9996e24bc43c548.png', 'YouTube', 'Streaming Service', 'Enjoy the videos and music you love, upload original content, and share it all with friends, family, and the world on YouTube.Enjoy the videos and music you love, upload original content, and share it', 'https://www.youtube.com/'),
(100, 'http://www.masonbruce.com/wp-content/uploads/2015/03/android-logo-transparent-background.png', 'Android', 'Mobile Software', 'See what\'s new with Android - from phones to watches and more. Visit the official site to explore and learn.See what\'s new with Android - from phones to watches and more. Visit the official site to ex', 'https://www.android.com/'),
(101, 'http://diylogodesigns.com/blog/wp-content/uploads/2017/07/uber-vector-logo.png', 'Uber', 'Taxi Service', 'Get a ride in minutes. Or become a driver-partner and earn money on your schedule. Uber is finding you better ways to move, work, and succeed.Get a ride in minutes. Or become a driver-partner and earn', 'https://www.uber.com/'),
(102, 'https://banner2.kisspng.com/20180409/ypw/kisspng-whatsapp-computer-icons-iphone-message-whatsapp-5acbdac007eae7.5003030315233092480324.jpg', 'WhatsApp', 'Messaging Service', 'Quickly send and receive WhatsApp messages right from your computer.Quickly send and receive WhatsApp messages right from your computer.Quickly send and receive WhatsApp messages right from your compu', 'https://www.whatsapp.com/');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Companies`
--
ALTER TABLE `Companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Projects`
--
ALTER TABLE `Projects`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Companies`
--
ALTER TABLE `Companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Projects`
--
ALTER TABLE `Projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
