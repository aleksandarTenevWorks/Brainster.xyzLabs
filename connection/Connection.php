<?php

class Connection
{
    private $user = 'root';
    private $pass = 'root';

    public function connection()
	{
    $db = new PDO("mysql:host=localhost:3306;dbname=brainsterLab;charset=utf8", $this->user, $this->pass);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return  $db;
    }
}