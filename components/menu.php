
<?php 
    include_once 'controlers/saveCompany.php';
 ?> 

    <!-- MENU -->
    <div class="navbar navbar-fixed-top  navMenu">
        <div class="container-fluid">
            <div class="navbar-header">
                <button class="navbar-toggle menuBtnCollapse" data-toggle="collapse" data-target="#menu">
                    <span class="collapseMenuLine icon-bar  "></span>
                    <span class="icon-bar collapseMenuLine"></span>
                    <span class="icon-bar collapseMenuLine"></span>
                </button>
                <div class="menuLogoDiv">
                    <img src="img/Brainster-Logo-03.png" alt="" class="logo">
                </div>  
            </div>
            <div class="collapse navbar-collapse navbar-right" id="menu">
                <ul class="nav navbar-nav navList">
                    <li>
                        <a href="http://codepreneurs.co/akademija-za-web-programiranje/" target="_blank">Академија за програмирање
                        </a>
                    </li>
                    <li>
                        <a href="http://www.brainster.io/marketpreneurs" target="_blank">Академија за маркетинг</a>
                    </li>
                    <li class="customLi">
                        <a href="https://blog.brainster.co/" target="_blank">Блог</a>
                    </li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#myModal" class="outlineNone">Вработи наши студенти</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Modal -->

    <div  id="myModal" class="modal fade <?php if(isset($_GET['emptyError']) || isset($_GET['sendSucces'])) { echo 'in'; ?> " <?php echo 'style="display:block"'; }?> role="dialog">
        <div class="modal-dialog form">
            <div class="modal-content ">
                <div class="modal-header formHeader">
                    <a href="brainsterLab.php" class="closeForm">&times;</a>
                    <img src="img/Brainster-Logo-03.png" alt="" class="logoForm">
                </div>
                <div class="modal-body formContent">
                    <h3> <?php if(isset($_GET['emptyError']))
                        {
                            echo $_GET['emptyError'];
                        }
                        if(isset($_GET['emailError']))
                        {
                            echo $_GET['emailError'];
                        }
                        if(isset($_GET['sendSucces']))
                        {
                            echo $_GET['sendSucces'];
                        }; ?>
                    </h3>
                    <h2>Контактирај не</h2>
                    <form action="brainsterLab.php" method="POST">
                        <div class="form-group">
                            <label for="email">Email:</label><br>
                            <input type="email" class=" formInput" id="email"  name="email" oninvalid="this.setCustomValidity('Полето е задолжително')"oninput="setCustomValidity('')" required><br>
                        </div>
                        <div class="form-group">
                            <label for="contact">Број за контакт:</label><br>
                            <input type="text" class=" formInput" id="contact" name="contact" oninvalid="this.setCustomValidity('Полето е задолжително')" oninput="setCustomValidity('')" required>
                        </div>
                        <div class="form-group">
                            <label for="companyName">Име на компанија:</label><br>
                            <input type="text" class="formInput" id="companyName"  name="companyName" oninvalid="this.setCustomValidity('Полето е задолжително')" oninput="setCustomValidity('')" required>
                        </div>
                        <button type="submit" class="btn btn-default formButton"  name="submit" >Контактирај не</button>
                    </form>
                </div>
                <div class="modal-footer">
                <span>Made with<span class="glyphicon glyphicon-heart"> </span> </span>

                </div>
            </div>
        </div>
    </div>
