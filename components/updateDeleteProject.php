
<?php
  require_once '../controlers/adminPanelProjectsController.php';
  session_start();
  if($_SESSION['logged'] == 'admin')
  {
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BrainsterLab Admin</title>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
    <link rel="stylesheet" href="../css/brainsterLabAdmin.css">
</head>
<body>
  <div class="wrapper">
    <h1>Brainster.xyz Lab Admin Panel</h1>
    <a href="addProjectPage.php" class="adminMenuBtn">Add New Project</a>
    <a href="updateDeleteProject.php" class="adminMenuBtn">Update or Delete Project</a>
    <a href="../controlers/logout.php" class="adminMenuBtn logoutBtn">Logout</a>
    <hr>
    <form  action="updateDeleteProject.php" method="POST">
      <div class="selectWrapper">
        <h3>Select Project to Update or Delete</h3>
        <select name="project" class="selectProject">
          <?php 
            foreach($arrayProjects as $value)
              {
          ?>
              <option  value="<?= $value['id']?>"><?= $value['title']?></option>
          <?php
              }
          ?>
        </select>
      </div>
      <button type="submit" name="update" class="btn btn-default btnAdminLogin marginRight10">Update</button>
      <button type="submit" name="delete" onclick="return confirm('Are you sure?');" class="btn btn-default btnAdminLogin">Delete</button>
    </form>
    <div class="hr"></div>
    <h2><?= $succesNote ?></h2>
      <?php
        if(isset($_POST['update']))
          {
            $rowID = (int) $_POST['project'];
            $rowObject = $listAllProjectsRadio->findRow($rowID);
      ?>
            <div class="form">
              <form action="updateDeleteProject.php" method="GET">
              <input type="hidden" name="showForm" >
                <input type="hidden" name="id" value="<?= $rowObject['id']?>">
                <div class="form-group">
                  <label for="picture">Project picture:</label>
                  <input type="text" class="form-control" placeholder="Enter picture url" name="updatePicture" value="<?= $rowObject['picture_url']?>" required>
                </div>
                <div class="form-group">
                  <label for="title">Project Title:</label>
                  <input type="text" class="form-control" placeholder="Enter project title" name="updateTitle" value="<?= $rowObject['title']?>" required>
                </div>
                <div class="form-group">
                  <label for="subtitle">Project Subtitle:</label>
                  <input type="text" class="form-control" placeholder="Enter project subtitle" name="updateSubtitle" value="<?= $rowObject['subtitle']?>" required>
                </div>
                <div class="form-group">
                    <label for="website">Project Website URL:</label>
                    <input type="text" class="form-control" placeholder="Enter project website URL" name="updateWebsite" value="<?= $rowObject['website_url']?>" required>
                  </div>
                <div class="form-group">
                  <label for="description">Project Description:</label><br>
                  <textarea maxlength="200" cols="20" rows="5" class="form-control" placeholder="Enter project description" name="updateDescription" ><?= $rowObject['description']?></textarea>
                </div>
                <button type="submit" class="btn btn-default btnAdminLogin" name="updateProjectBtn">Update Project</button>
              </form>
            </div>
      <?php
        }
      ?>
</body>
</html>
<?php
  } else {
    header('Location:../brainsterLabAdminLogin.php');
  }
?>