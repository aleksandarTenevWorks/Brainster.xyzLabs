
    <!-- FOOTER -->
    <div class="container-fluid">
        <div class="row footerRow">
            <div class="footerContent">
                <div>Made with<span class="glyphicon glyphicon-heart"> </span> by</div>
                <div class="footerLogo">
                    <img src="img/Brainster-Logo-03.png" alt="">
                </div>
                <div>-
                    <a href="https://www.facebook.com/brainster.co/" class="fbLink">Say Hi!</a> -
                    <span class="terms"> Terms</span>
                </div>
            </div>
        </div>
    </div>

   
