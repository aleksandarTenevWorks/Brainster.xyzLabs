
<?php
  require_once '../controlers/addProjectDb.php';
  session_start();
  if($_SESSION['logged'] == 'admin')
  {
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BrainsterLab Admin</title>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous"> 
    <link rel="stylesheet" href="../css/brainsterLabAdmin.css">
</head>
<body>
    <div class="wrapper">
      <h1>Brainster.xyz Lab Admin Panel</h1>
      <a href="addProjectPage.php" class="adminMenuBtn">Add Project</a>
      <a href="updateDeleteProject.php" class="adminMenuBtn">Update or Delete Project</a>
      <a href="../controlers/logout.php" class="adminMenuBtn logoutBtn">Logout</a>
      <hr>
      <h2><?= $succesNote ?></h2>
      <h2><?= $emptyFieldError ?></h2>
      <h2><?= $wrongUrlFormat ?></h2>
      <div class="form">
        <h2>Add project</h2>
        <form action="addProjectPage.php" method="POST">
          <div class="form-group">
            <label for="picture">Project picture:</label>
            <input type="text" class="form-control" placeholder="Enter picture url" name="picture" required>
          </div>
          <div class="form-group">
            <label for="title">Project Title:</label>
            <input type="text" class="form-control" placeholder="Enter project title" name="title" required>
          </div>
          <div class="form-group">
            <label for="subtitle">Project Subtitle:</label>
            <input type="text" class="form-control" placeholder="Enter project subtitle" name="subtitle" required>
          </div>
          <div class="form-group">
            <label for="website">Website URL:</label>
            <input type="text" class="form-control" placeholder="Enter project website URL" name="website" required>
          </div>
          <div class="form-group">
            <label for="description">Project Description:</label>
            <textarea maxlength="200" cols="20" rows="5" class="form-control" placeholder="Enter project description" name="description" ></textarea>      
          </div>
          <button type="submit" class="btn btn-default btnAdminLogin" name="addProjectBtn">Add Project</button>
        </form>
      </div>
    </div>
    <a href="../controlers/logout.php">Logout</a>
</body>
</html>
<?php
      } else {
        header('Location:../brainsterLabAdminLogin.php');
      }
?>