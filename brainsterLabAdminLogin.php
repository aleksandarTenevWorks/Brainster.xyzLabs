<?php
  $loginRequest = '/var/www/html/Brainster Labs/controlers/adminLoginValidationCheck.php';
  require_once ($loginRequest);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BrainsterLab Admin</title>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/brainsterLabAdmin.css">
</head>
<body>
    <div class="wrapper">
      <h1>Brainster Labs.xyz Admin Panel</h1>
      <div class="form">
        <h3>Admin Login</h3>
        <form action="brainsterLabAdminLogin.php" method="POST" class="adminLogin">
          <div class="form-group">
            <label for="user">Admin:</label>
            <input type="text" class="form-control"  placeholder="Enter admin" name="user" required>
            <p><?= $validate->getErrorUser() ?></p>
          </div>
          <div class="form-group">
            <label for="pass">Password:</label>
            <input type="password" class="form-control" placeholder="Enter password" name="pass" required>
            <p><?= $validate->getErrorPass() ?></p>
          </div>
          <button type="submit" class="btn btn-default btnAdminLogin" name="adminLoginBtn">Login</button>
        </form>
      </div>
    </div> 
</body>
</html>